# Exercício sobre WebSockets

[Exercicio 1 CLICK AQUI](#exercício-1)  
[Exercicio 2 CLICK AQUI](#exercício-2)  

## Exercício 1
### O servidor envia regularmente, de segundo em segundo, uma mensagem com o horário do servidor  
Funcionamento
![Funcionamento](prints/funcionamento.png) 

1. Foi implementado o seguinte cógido no servidor backend: 
```
setInterval(() => {
    let time = `{"time":"${Date().substring(16,24)}"}`
    wss.clients.forEach(function each(client) {
      if (client === ws && client.readyState === WebSocket.OPEN) {
        client.send(time)
      }
    });
  }, 1000);
```
server.js
![servidor](prints/server_backend.png) 

2. Foi criado o componente TimeServer.js contendo o seguinte código
```
import React from 'react'

export default class TimeServer extends React.Component{
    render(){
        return(
            <div>
                <h1>Horário do Servidor: {this.props.time}</h1>
            </div>
        )
    }
    
}
```
TimerServer.js
![TimeServer.js](prints/TimeServer.png) 

3. Foi importado o componente TimeServer.js no arquivo Chat.js e adicionado o atributo time ao state sendo este inicializado com o horário do servidor frontend, o qual é em seguida atualizado pelo horário do server backend  


![Importação e inclusão time atributo no state](prints/alt1chat.png)

4. Foi adicionado à função ``this.ws.onmessage`` a mudança de estado o atributo ``time`` para o valor time recebido da mensagem do servidor ``this.setState({time: message.time})``
![Alteração onmessage com mudança de estado](prints/altchat2.png)

5. Por fim foi incluído o componente TimeServer na função render() do Chat.js passando por parâmetro o estado do time `` <TimeServer time={this.state.time}/>``  
![Inclusão TimeServe componente](prints/altchat3.png)

## Exercício 2
### O servidor recebe uma expressão aritmética e retorna o resultado. Por exemplo, 2+3, o servidor deve retornar 5 

Funcionamento
![Funcionamento](prints/funcionamentomath.png)

1. Foi modificada a função ``ws.on('message', function incoming(data)`` no server.js do backend conforme abaixo:
```
ws.on('message', function incoming(data) {
    const regex = /[-+]?[0-9][ ]?[-*\/+][ ]?[-+]?[0-9]/
    if(JSON.parse(data).message.match(regex)){
      let value = `{"name":"${JSON.parse(data).name}","expressao":"${JSON.parse(data).message}",
      "result":"${eval(JSON.parse(data).message)}","time":"${Date().substring(16,24)}"}`
      // console.log(value)
      wss.clients.forEach(function each(client){
        if (client.readyState === WebSocket.OPEN) {
          client.send(value)
        }
      })
     
    } else{
      wss.clients.forEach(function each(client) {
        if (client !== ws && client.readyState === WebSocket.OPEN) {
          
          client.send(data)
        }
      });
    }
    
  });
  ```
![server.js](prints/servermath1.png) 

2. Foi alterada a função ``submitMessage`` do arquivo Chat.js para adicionar a mensagem só quando não for expressão matemática
```
if(!message.message.match(/[-+]?[0-9][ ]?[-*\/+][ ]?[-+]?[0-9]/))
      this.addMessage(message)
```

3. Por fim foi alterado o componente ``ChatMessage`` do render do Chat.js conforme abaixo
```
<ChatMessage
  key={index}
  message={message.result ? message.expressao + " = " + message.result : message.message}
  name={message.name}
/>,
``` 
![Chat.js](prints/chatmath.png)


