const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 3030 });

wss.on('connection', function connection(ws) {

  setInterval(() => {
    let time = `{"time":"${Date().substring(16,24)}"}`
    wss.clients.forEach(function each(client) {
      if (client === ws && client.readyState === WebSocket.OPEN) {
        client.send(time)
      }
    });
  }, 1000);

  ws.on('message', function incoming(data) {
    const regex = /[-+]?[0-9][ ]?[-*\/+][ ]?[-+]?[0-9]/
    if(JSON.parse(data).message.match(regex)){
      let value = `{"name":"${JSON.parse(data).name}","expressao":"${JSON.parse(data).message}",
      "result":"${eval(JSON.parse(data).message)}","time":"${Date().substring(16,24)}"}`
      // console.log(value)
      wss.clients.forEach(function each(client){
        if (client.readyState === WebSocket.OPEN) {
          client.send(value)
        }
      })
     
    } else{
      wss.clients.forEach(function each(client) {
        if (client !== ws && client.readyState === WebSocket.OPEN) {
          
          client.send(data)
        }
      });
    }
    
  });

});

